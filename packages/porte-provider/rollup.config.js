import nodeResolve from "@rollup/plugin-node-resolve";
import json from "@rollup/plugin-json";
import nodePolyfills from "rollup-plugin-node-polyfills";
import globals from "rollup-plugin-node-globals";
import commonjs from "@rollup/plugin-commonjs";
import esbuild from "rollup-plugin-esbuild";
import alias from "@rollup/plugin-alias";

export default [
  {
    input: "./src/index.ts",
    output: [
      {
        file: "./dist/index.umd.js",
        name: "Porte",
        format: "umd",
        sourcemap: true,
      },
      {
        file: "./dist/index.es.js",
        format: "esm",
        sourcemap: true,
      },
      {
        file: "./dist/index.cjs.js",
        format: "cjs",
        sourcemap: true,
      },
    ],
    plugins: [
      alias({
        entries: {
          "string_decoder/":
            "rollup-plugin-node-polyfills/polyfills/string-decoder",
          "events/": "rollup-plugin-node-polyfills/polyfills/events",
        },
      }),
      esbuild(),
      nodePolyfills(),
      nodeResolve({
        browser: true,
        preferBuiltins: true
      }),
      commonjs(),
      globals({
        global: true,
        buffer: true,
        process: true
      }),

      json(),
    ],
  },
];
