import ProviderEngine from "web3-provider-engine";
import { Provider } from "ethereum-types";

const CacheSubprovider = require("web3-provider-engine/subproviders/cache.js");
const FixtureSubprovider = require("web3-provider-engine/subproviders/fixture.js");
const FilterSubprovider = require("web3-provider-engine/subproviders/filters.js");
const HookedWalletSubprovider = require("web3-provider-engine/subproviders/hooked-wallet.js");
const NonceSubprovider = require("web3-provider-engine/subproviders/nonce-tracker.js");
const VmSubprovider = require("web3-provider-engine/subproviders/vm.js");
const RpcSubprovider = require("web3-provider-engine/subproviders/rpc.js");
const SubscriptionsSubprovider = require("web3-provider-engine/subproviders/subscriptions.js");
const SanitizingSubprovider = require("web3-provider-engine/subproviders/sanitizer.js");
const InflightCacheSubprovider = require("web3-provider-engine/subproviders/inflight-cache.js");

export class PorteSubProvider {
  private engine?: ProviderEngine;
  private network?: NetworkType;
  private infuraId?: string;
  private rpcUrl?: string;
  public createProviderEngine(
    options: PorteSubProviderOptions
  ): Promise<Provider> {
    this.network = options.network || "mainnet";
    this.infuraId = options.infuraId || "1c71b7f01d194ff985ca006da807a48f";
    console.log(this.infuraId, this.network);
    this.rpcUrl = `https://${this.network}.infura.io/v3/${this.infuraId}`;

    this.engine = new ProviderEngine({
      pollingInterval: options.pollingInterval || 15000,
    });
    // static results
    this.engine.addProvider(
      new FixtureSubprovider({
        web3_clientVersion: "ProviderEngine/v0.0.0/javascript",
        net_listening: true,
        eth_hashrate: "0x00",
        eth_mining: false,
        eth_syncing: true,
      })
    );

    // cache layer
    this.engine.addProvider(new CacheSubprovider());

    this.engine.addProvider(new SanitizingSubprovider());

    this.engine.addProvider(new SubscriptionsSubprovider());

    this.engine.addProvider(new InflightCacheSubprovider());

    // filters  (requires a stateful data api)
    this.engine.addProvider(new FilterSubprovider());

    // pending nonce
    this.engine.addProvider(new NonceSubprovider());

    // vm
    this.engine.addProvider(new VmSubprovider());

    // id mgmt
    this.engine.addProvider(
      new HookedWalletSubprovider({
        getAccounts: function (cb: CallableFunction) {
          /** todo */
        },
        approveTransaction: function (cb: CallableFunction) {
          /** todo */
        },
        signTransaction: function (cb: CallableFunction) {
          /** todo */
        },
      })
    );

    // data source
    this.engine.addProvider(
      new RpcSubprovider({
        rpcUrl: this.rpcUrl,
      })
    );

    return Promise.resolve(this.startEngine(this.engine));
  }

  private startEngine(engine: ProviderEngine) {
    // log new blocks
    // @ts-ignore
    this.engine.on("block", (block: any) => {
      console.log("================================");
      console.log(
        "BLOCK CHANGED:",
        "#" + block.number.toString("hex"),
        "0x" + block.hash.toString("hex")
      );
      console.log("================================");
    });

    // network connectivity error
    // @ts-ignore
    this.engine.on("error", function (err) {
      // report connectivity errors
      console.error(err.stack);
    });
    // start polling for blocks
    engine.start();
    return engine;
  }
}

export type NetworkType =
  | "mainnet"
  | "ropsten"
  | "kovan"
  | "rinkeby"
  | "goerli"
  | "palm-mainnet"
  | "palm-testnet"
  | "aurora-mainnet"
  | "aurora-testnet"
  | "near-mainnet"
  | "near-testnet";

export interface PorteSubProviderOptions {
  network?: NetworkType;
  infuraId?: string;
  pollingInterval?: number;
}

export const Porte = PorteSubProvider.prototype;

if (typeof window !== "undefined") {
  (window as any).Porte = new PorteSubProvider();
}
