import React from "react";
import { Porte } from "@porte/web3-provider";
import { useEffect, useState } from "react";
import Web3 from 'web3'

function App() {
  const address = "0x704F4eDc12810f554A8Cc4509b278890De201272"
  const [engine, setEngine] = useState<any>();
  const [balance, setBalance] = useState('-')

  useEffect(() => {
    Porte.createProviderEngine({
      network: "ropsten"
    }).then((eg) => {
      setEngine(eg);
    });
  }, []);

  const onCheckBalance = async () => {
    const web3 = new Web3(engine)
    let balance = await web3.eth.getBalance(address)
    balance = web3.utils.fromWei(Web3
      .utils.toBN(balance), 'ether').toString()
    setBalance(balance)
    

  }

  return <div className="App">
    <p>Address: { address }</p>
    <p>Balance: { balance }</p>
    <button onClick={onCheckBalance}>Check</button>
  </div>;
}

export default App;
